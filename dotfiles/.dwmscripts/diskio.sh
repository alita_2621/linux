#!/bin/bash

write=`awk '/sda / {print $10 / 2 / (1024*1024)}' /proc/diskstats`
read=`awk '/sda / {print $6 / 2 / (1024*1024)}' /proc/diskstats`

printf "R "
sumread=$read
outread=$(echo $sumread | cut -b 1-4)
printf  "${outread}G"

printf  " W " 
inwrite=$write
outwrite=$(echo $inwrite | cut -b 1-4 )
printf "${outwrite}G"



