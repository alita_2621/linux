import datetime
import os
import time
from timeloop import Timeloop
from datetime import timedelta


tl = Timeloop()



@tl.job(interval=timedelta(seconds=1))
def status():
    start = time.time()
    
    date = datetime.datetime.now().strftime("%X")
    
    diskiowr = os.popen("bash ~/.config/polybar/script/diskio-wr.sh").read()
    intusage = os.popen("bash ~/.config/polybar/script/intusage.sh").read()
    bandwidth = os.popen("bash ~/.config/polybar/script/bandwidth.sh").read()
    mem = os.popen("bash  ~/.config/polybar/script/mem.sh").read()
    cpu = os.popen("bash  ~/.config/polybar/script/cpu.sh").read()
    term = os.popen("bash  ~/.config/polybar/script/term.sh").read()
    psstatus = os.popen("bash  ~/.config/polybar/script/psstatus.sh").read()


    status =  ' 〱 '  + term + ' 〱 '  + psstatus   + ' 〱'  + cpu   + ' 〱 '  + mem  +   ' 〱 '  + bandwidth + ' 〱 ' + diskiowr + ' 〱 ' + date  


    os.system('xsetroot -name "' + status + '"' )

    
  #  done = time.time()
 #   elapsed = done - start
##    print("date ",  elapsed)






if __name__ == "__main__":
    tl.start(block=True)
