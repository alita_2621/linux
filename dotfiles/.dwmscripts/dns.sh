 INTERFACE=$(ip route | awk '/^default/ { print $5 ; exit }')


if [ -z $INTERFACE ]
then
    echo "  D  "
    exit
else
    ping=$( ping -c1 1.1.1.1 | grep time | head -n1 | awk '{printf " " $    7}' | sed "s/time=//g" | tr "." "\n" | head -n1)
   
  
    if [ $ping == "errors," ]
    then
        echo "E"
    fi
    
    
    if [ $ping == "packet" ]
    then
        echo "P"
    fi
    
    echo $ping

fi
 
  
