function twitch.dowland


  set res (echo -e "160p\n360p\n480p\n720p\n720p60\n1080p60" | fzf)
  set videoid (string split "videos/" $argv[1] | tail -n 1) 
  
  echo $videoid

  if test -z $argv[2]
      streamlink "$argv[1]" $res -o "$videoid-$res"
    else
      streamlink "$argv[1]" $res  -o "$videoid-$res-.mp4" --hls-start-offset $argv[2] 
  end
end

