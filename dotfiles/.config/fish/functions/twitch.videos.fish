function twitch.videos



set data (cat ~/.config/fish/functions/stream | fzf | awk '{printf $2}' )
set res (echo -e "160p\n360p\n480p\n720p\n720p60\n1080p60" | fzf)

 curl -s -H 'Accept: application/vnd.twitchtv.v5+json' -H 'Client-ID: '"$twitchtoken"' '  -X GET 'https://api.twitch.tv/kraken/channels/'"$data"'/videos' | jq ".[]" | jq -r '.[] | .url + " :" + .title' | fzf | awk '{print $1}' | xargs -I% streamlink --player "mpv" % $res 

end
