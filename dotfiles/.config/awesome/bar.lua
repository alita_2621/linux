local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")


awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
 --   set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = awful.util.taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons
    }

  
  local volumebar_widget = require("module.volumearc")
  local brightness_widget = require("module.brightnessarc")
  local cpu_widget = require("module.cpu")
	local mem_widget = require("module.mem")
  local swap_widget = require("module.swap")
  local test_widget = require("module.test")
  local cpu_top_widget = require("module.cpu-widget")  
  local net_speed_widget = require("module.net-speed")
  local disk_speed_widget = require("module.disk-speed")
  local date_widget = require("module.date")
  local ps_widget = require("module.ps")
  local temp_widget = require("module.temp")
  local cpuhz_widget = require("module.cpuhz")
  
  s.mywibox = awful.wibar({ position = "top", screen = s , height = 22  })
    
  -- Create the wibox
  s.split = wibox.widget.textbox(string.format(' <span color="%s"> // </span>', "#555555"))
  s.space = wibox.widget.textbox(string.format(' <span color="%s">   </span>', "#555555"))
 	
  
  -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        expand = 'none',
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            s.mytaglist,
            s.mylayoutbox,
            s.mypromptbox,
          },
        {
            layout = wibox.layout.fixed.horizontal,
            
            s.split,
            temp_widget(),
            s.split,
            cpuhz_widget(),
            s.split,
            ps_widget(),
            s.split,
            cpu_widget({cpu="Cpu0"}),
            cpu_widget({cpu="Cpu1"}),
            cpu_widget({cpu="Cpu2"}),
            cpu_widget({cpu="Cpu3"}),
            cpu_top_widget({
              width = 70,
              step_width = 2,
              step_spacing = 0,
              enable_kill_button = true,
              color = "#7CB7FF"
            }),
            s.split,
            mem_widget(),
            swap_widget(),
            s.split,
            net_speed_widget(),
            s.split,
            disk_speed_widget(),
            s.split,
        },
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            wibox.widget.systray(),
 
            s.split,
            brightness_widget(),
            s.space,
            volumebar_widget(),
            s.split,
            date_widget(),
		},
    }
end)
-- }}}




