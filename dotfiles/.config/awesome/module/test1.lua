
local awful = require("awful")
local wibox = require("wibox")
local watch = require("awful.widget.watch")
local spawn = require("awful.spawn")
local beautiful = require("beautiful")


local widget = {}
local cpu_arc_widget = {}

  local cpu_rows = {
      spacing = 4,
      layout = wibox.layout.fixed.vertical,
  }
  local is_update = true
  local process_rows = {
      layout = wibox.layout.fixed.vertical,
  }

  local function split(string_to_split, separator)
      if separator == nil then separator = "%s" end
      local t = {}
  
      for str in string.gmatch(string_to_split, "([^".. separator .."]+)") do
          table.insert(t, str)
      end
  
      return t
  end
  
  -- Checks if a string starts with a another string
  local function starts_with(str, start)
      return str:sub(1, #start) == start
  end
  
  
  local function create_textbox(args)
      return wibox.widget{
          text = args.text,
          align = args.align or 'left',
          markup = args.markup,
          forced_width = args.forced_width or 40,
          widget = wibox.widget.textbox
      }
    end

local function create_process_header(params)
      local res = wibox.widget{
          create_textbox{markup = '<b>PID</b>'},
          create_textbox{markup = '<b>Name</b>'},
          {
              create_textbox{markup = '<b>%CPU</b>'},
              create_textbox{markup = '<b>%MEM</b>'},
              params.with_action_column and create_textbox{forced_width = 20} or nil,
              layout = wibox.layout.align.horizontal
          },
          layout  = wibox.layout.ratio.horizontal
      }
      res:ajust_ratio(2, 0.2, 0.47, 0.33)
  
      return res
  end




  local function create_kill_process_button()
      return wibox.widget{
          {
              id = "icon",
              image = WIDGET_DIR .. '/window-close-symbolic.svg',
              resize = false,
              opacity = 0.1,
              widget = wibox.widget.imagebox
          },
          widget = wibox.container.background
      }
  end




local function worker(args)

    local args = args or {}

    local color = args.color or beautiful.fg_color
    local bg_color = args.bg_color or '#ffffff11'
    local timeout = args.timeout or 1

    local width = args.width or 50
    local step_width = args.step_width or 2
    local step_spacing = args.step_spacing or 1
    local enable_kill_button = args.enable_kill_button or false
   local process_info_max_length = args.process_info_max_length or -1


    cpu_arc_widget = wibox.widget {
        max_value = 1.1,
        thickness = 3,
        start_angle = 4.71238898, -- 2pi*3/4
        forced_height = 18,
        forced_width = 18,
        bg = bg_color,
        paddings = 2,
        colors = { 
            '#74aeab', '#26403f',
            '#74aeab', '#26403f',
            '#74aeab', '#26403f',
            '#74aeab', '#26403f',
        },
        widget = wibox.container.arcchart
    }


    local cpu0, cpu1, cpu2, cpu3 
    
    watch('bash -c "bash /home/alita/.scripts/cpu.sh"', timeout,
        function(cpu_arc_widget, stdout, stderr, exitreason, exitcode)
            local cpu0=stdout.sub(stdout,1,4)
            local cpu1=stdout.sub(stdout,6,9)
            local cpu2=stdout.sub(stdout,11,14)
            local cpu3=stdout.sub(stdout,16,19)
 

            --print(   cpu0 , 1 - cpu0 , cpu1 , 1 - cpu1 , cpu2 ,  1 - cpu2  , cpu3 , 1 - cpu3 )

            cpu_arc_widget.values = { cpu0 , 1 - cpu0 , cpu1 , 1 - cpu1 , cpu2 ,  1 - cpu2  , cpu3 , 1 - cpu3 }  
        end,
        cpu_arc_widget
    )









    return cpu_arc_widget
end

return setmetatable(cpu_arc_widget, { __call = function(_, ...)return worker(...) end })
