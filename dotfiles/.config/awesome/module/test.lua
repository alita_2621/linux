
local awful = require("awful")
local watch = require("awful.widget.watch")
local wibox = require("wibox")
local beautiful = require("beautiful")
local gears = require("gears")

local widget = {}

local function worker(args)

    local args = args or {}
    local timeout = args.timeout or 1

    local cpugraph_widget = wibox.widget{
    markup = 'This <i>is</i> a <b>textbox</b>!!!',
    align  = 'center',
    valign = 'center',
    widget = wibox.widget.textbox
}



local awk_test = "'{print ($10 * 36 )}'"
    watch('bash -c "top -b -n1 -p `pgrep -d "," awesome` | grep "awesome" | awk '.. awk_test ..' "', 30 ,
        function(widget, stdout, stderr, exitreason, exitcode)
print(stdout)
            widget.markup = stdout

        end,
        cpugraph_widget
    )





    return cpugraph_widget
end

return setmetatable(widget, { __call = function(_, ...)
    return worker(...)
end })
