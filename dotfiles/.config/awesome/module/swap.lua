local awful = require("awful")
local watch = require("awful.widget.watch")
local wibox = require("wibox")

local date_widget = {}

local function worker(args)

    local args = args or {}
    local timeout = args.timeout or 1

    --- Main ram widget shown on wibar
    date_widget = wibox.widget {
        widget = wibox.widget.textbox
      }


    watch([[
      bash -c "free | head -n 3 | tail -n 1 | awk '{print $2}'"
    ]], timeout,
        function(widget, stdout, stderr, exitreason, exitcode)
            widget:set_text(string.format("%.3f",stdout))
          end,
        date_widget
    )


    return date_widget
end

return setmetatable(date_widget, { __call = function(_, ...)
    return worker(...)
end })
