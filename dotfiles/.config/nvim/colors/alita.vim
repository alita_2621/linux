
highlight clear

if exists("syntax_on")
  syntax reset
endif

set t_Co=256
let g:colors_name = "alita"

hi! EndOfBuffer ctermbg=NONE ctermfg=NONE 


hi Normal               guifg=#f8f8f2   guibg=NONE   gui=NONE

hi Directory            guifg=#ae81ff   guibg=NONE      gui=NONE
hi Folded               guifg=#75715e   guibg=#272822   gui=NONE
hi SignColumn           guifg=NONE      guibg=#3c3d37   gui=NONE

hi String               guifg=#71f79f   guibg=NONE      gui=NONE
hi Label                guifg=#71f79f   guibg=NONE      gui=NONE

hi Todo                 guifg=#75715e   guibg=NONE      gui=inverse,bold
hi Comment              guifg=#75715e   guibg=NONE      gui=NONE

hi Boolean              guifg=#c74ded   guibg=NONE      gui=NONE
hi Character            guifg=#c74ded   guibg=NONE      gui=NONE
hi Float                guifg=#c74ded   guibg=NONE      gui=NONE
hi Number               guifg=#7D3C98    guibg=NONE      gui=NONE

hi Conditional          guifg=#F92672   guibg=NONE      gui=NONE
hi Define               guifg=#F92672   guibg=NONE      gui=NONE
hi Keyword              guifg=#F92672   guibg=NONE      gui=NONE
hi Operator             guifg=#F92672   guibg=NONE      gui=NONE
hi PreProc              guifg=#F92672   guibg=NONE      gui=NONE
hi Tag                  guifg=#F92672   guibg=NONE      gui=NONE
hi Statement            guifg=#f92672   guibg=NONE      gui=NONE
hi Type                 guifg=#F92672   guibg=NONE      gui=NONE

hi Constant             guifg=NONE      guibg=NONE      gui=NONE

hi Function             guifg=#71f79f   guibg=NONE      gui=NONE
hi Identifier           guifg=#7cb7ff   guibg=NONE      gui=italic
hi NonText              guifg=#49483e   guibg=#31322c   gui=NONE


hi ErrorMsg             guifg=#f8f8f0   guibg=#f92672   gui=NONE
hi WarningMsg           guifg=#f8f8f0   guibg=#f92672   gui=NONE

hi StorageClass         guifg=#00c1e4   guibg=NONE      gui=italic
hi Title                guifg=#f8f8f2   guibg=NONE      gui=bold
hi Underlined           guifg=NONE      guibg=NONE      gui=underline


hi StatusLine           guifg=#f8f8f2   guibg=#64645e   gui=bold
hi StatusLineNC         guifg=#f8f8f2   guibg=#64645e   gui=NONE


hi Cursor               guifg=#272822   guibg=#f8f8f0   gui=NONE
hi Visual               guifg=NONE      guibg=#49483e   gui=NONE

hi CursorLine           guifg=NONE      guibg=#3c3d37   gui=NONE
hi CursorColumn         guifg=NONE      guibg=#3c3d37   gui=NONE
hi ColorColumn          guifg=NONE      guibg=#3c3d37   gui=NONE

hi LineNr               guifg=#90908a   guibg=#3c3d37   gui=NONE
hi VertSplit            guifg=#64645e   guibg=#64645e   gui=NONE
hi MatchParen           guifg=#f92672   guibg=NONE      gui=underline

hi Special              guifg=#f8f8f2   guibg=NONE      gui=NONE
hi SpecialComment       guifg=#75715e   guibg=NONE      gui=NONE
hi SpecialKey           guifg=#49483e   guibg=#3c3d37   gui=NONE

hi Pmenu                guifg=NONE      guibg=#222222    gui=NONE
hi PmenuSel             guifg=NONE      guibg=#49483e   gui=NONE



hi DiffAdd              guifg=#f8f8f2   guibg=#46830c   gui=bold
hi DiffDelete           guifg=#8b0807   guibg=NONE      gui=NONE
hi DiffChange           guifg=#f8f8f2   guibg=#243955   gui=NONE
hi DiffText             guifg=#f8f8f2   guibg=#204a87   gui=bold




hi IncSearch            guifg=#C4BE89 guibg=#000000
hi Search               gui=NONE guifg=#f8f8f2 guibg=#204a87





hi htmlTag ctermfg=148 ctermbg=NONE cterm=NONE guifg=#a6e22e guibg=NONE gui=NONE
hi htmlEndTag ctermfg=148 ctermbg=NONE cterm=NONE guifg=#a6e22e guibg=NONE gui=NONE
hi htmlTagName ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi htmlArg ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi htmlSpecialChar ctermfg=141 ctermbg=NONE cterm=NONE guifg=#ae81ff guibg=NONE gui=NONE

hi javaScriptFunction ctermfg=81 ctermbg=NONE cterm=NONE guifg=#66d9ef guibg=NONE gui=italic
hi javaScriptRailsFunction ctermfg=81 ctermbg=NONE cterm=NONE guifg=#66d9ef guibg=NONE gui=NONE
hi javaScriptBraces ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE

hi yamlKey ctermfg=197 ctermbg=NONE cterm=NONE guifg=#f92672 guibg=NONE gui=NONE
hi yamlAnchor ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi yamlAlias ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi yamlDocumentHeader ctermfg=186 ctermbg=NONE cterm=NONE guifg=#e6db74 guibg=NONE gui=NONE

hi cssURL ctermfg=208 ctermbg=NONE cterm=NONE guifg=#fd971f guibg=NONE gui=italic
hi cssFunctionName ctermfg=81 ctermbg=NONE cterm=NONE guifg=#66d9ef guibg=NONE gui=NONE
hi cssColor ctermfg=141 ctermbg=NONE cterm=NONE guifg=#ae81ff guibg=NONE gui=NONE
hi cssPseudoClassId ctermfg=148 ctermbg=NONE cterm=NONE guifg=#a6e22e guibg=NONE gui=NONE
hi cssClassName ctermfg=148 ctermbg=NONE cterm=NONE guifg=#a6e22e guibg=NONE gui=NONE
hi cssValueLength ctermfg=141 ctermbg=NONE cterm=NONE guifg=#ae81ff guibg=NONE gui=NONE
hi cssCommonAttr ctermfg=81 ctermbg=NONE cterm=NONE guifg=#66d9ef guibg=NONE gui=NONE
hi cssBraces ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE

