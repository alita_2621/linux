export ZSH="/home/alita/.oh-my-zsh"

ZSH_THEME="bira"

DISABLE_UPDATE_PROMPT="true" # true
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"

alias c.cache='sudo sh -c "echo 3 >"/proc/sys/vm/drop_caches" "'
alias c.swap='sudo swapoff -a  && sudo swapon -a '
alias ytdl.audio='youtube-dl --ignore-errors --output "%(title)s.%(ext)s" --extract-audio --audio-format mp3'
alias ytdl.audio.playlist='youtube-dl -o "%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s" --extract-audio --audio-format mp3'
alias yt='youtube-viewer'
alias music='mpv --shuffle ~/99/audio/newmusic/*'

alias pkg.update='sudo apt-get update && sudo apt-get upgrade'
alias pkg.install='sudo apt-get install'
alias pkg.remove='sudo apt-get remove'

#alias pkg.update='sudo pacman -Syu'
#alias pkg.install='sudo pacman -S'
#alias pkg.remove='sudo pacman -R'


atest() {
  echo 'test ' $1
}




plugins=(git colored-man-pages)

source $ZSH/oh-my-zsh.sh
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
