cpudata=$(top -1 -b -n1 | head -n 6 | tail -n 4 )

cpu0=$(echo -e $cpudata | sed 's/st /|/g' | tr '|' '\n'  | grep "Cpu0" | awk '{printf "%.2f" , ($3 + $5) / 100 }' )
cpu1=$(echo -e $cpudata | sed 's/st /|/g' | tr '|' '\n'  | grep "Cpu1" | awk '{printf "%.2f" , ($3 + $5) / 100 }' )
cpu2=$(echo -e $cpudata | sed 's/st /|/g' | tr '|' '\n'  | grep "Cpu2" | awk '{printf "%.2f" , ($3 + $5) / 100 }' )
cpu3=$(echo -e $cpudata | sed 's/st /|/g' | tr '|' '\n'  | grep "Cpu3" | awk '{printf "%.2f" , ($3 + $5) / 100 }' )

echo $cpu0 $cpu1 $cpu2 $cpu3 
