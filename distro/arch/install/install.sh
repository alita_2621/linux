
#!/bin/bash

source install.conf


## pacman config

pacman -Sy --noconfirm reflector
reflector --sort rate -p http -p https -c SE -c TR -c BG -c UA  -f 5 --verbose --save /etc/pacman.d/mirrorlist

sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf
sed -i "/\[Color\]/,/Include/"'s/^#//' /etc/pacman.conf
sed -i "/\[TotalDownload\]/,/Include/"'s/^#//' /etc/pacman.conf
sed -i '/TotalDowland/a ILoveCandy' /etc/pacman.conf 


echo "/etc/pacman.d/mirrorlist------------------------------------"
cat /etc/pacman.d/mirrorlist

read -p "enter" 



## Configere
ln -sf /usr/share/zoneinfo/Europe/Istanbul /etc/localtime
hwclock --systohc
locale-gen
echo en_US.UTF-8 UTF-8 > /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
echo KEYMAP=trq > /etc/vconsole.conf
echo $archpcname > /etc/hostname
echo -e "127.0.0.1\tlocalhost\n::1\tlocalhost\n127.0.1.1\t$archpcname.localdomain\t$archpcname" > /etc/hosts


## BOOT
mkinitcpio -p linux
pacman -S --noconfirm intel-ucode


    bootctl install
    echo default arch > /boot/loader/loader.conf
    rootuuid=$(lsblk -f | grep $archroot | awk '{print $4}')
    echo $rootuuid

    echo -e "title \t arch
linux \t /vmlinuz-linux
initrd \t /intel-ucode.img
initrd \t /initramfs-linux.img
options\troot=UUID=$rootuuid rw" > /boot/loader/entries/arch.conf


## görüntü sunucusu xorg 
pacman -Sy --noconfirm \
 xorg-server xorg-xinit xf86-video-intel \
 cpupower \
 networkmanager network-manager-applet \
 ufw \
 pulseaudio pavucontrol pulsemixer \
 dmenu rofi \
 konsole \
 awesome\
 qutebrowser \
 nemo pcmanfm ranger dolphin ffmpegthumbs \
 vlc mpv youtube-dl \
 htop ksysguard \
 flameshot gparted nitrogen lxappearance 




echo -e "governor='performance'\nmax_freq="2.50Ghz"" > /etc/default/cpupower

## servisler 

systemctl enable NetworkManager
systemctl enable cpupower
systemctl enable ufw
ufw enable


sed -i '82 s/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/g' /etc/sudoers


useradd -m -G wheel -s /usr/bin/fish $archuser

echo "root passwd"
passwd

echo  $archuser "passwd"
passwd $archuser


