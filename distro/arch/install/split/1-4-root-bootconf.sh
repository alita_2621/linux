
#!/bin/bash

source install.conf


## Configere
ln -sf /usr/share/zoneinfo/Europe/Istanbul /etc/localtime
hwclock --systohc
locale-gen
echo en_US.UTF-8 UTF-8 > /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
echo KEYMAP=trq > /etc/vconsole.conf
echo $archpcname > /etc/hostname
echo -e "127.0.0.1\tlocalhost\n::1\tlocalhost\n127.0.1.1\t$archpcname.localdomain\t$archpcname" > /etc/hosts


## BOOT
mkinitcpio -p linux
pacman -S --noconfirm intel-ucode


    bootctl install
    echo default arch > /boot/loader/loader.conf
    rootuuid=$(lsblk -f | grep $archroot | awk '{print $4}')
    echo $rootuuid

    echo -e "title \t arch
linux \t /vmlinuz-linux
initrd \t /intel-ucode.img
initrd \t /initramfs-linux.img
options\troot=UUID=$rootuuid rw" > /boot/loader/entries/arch.conf






