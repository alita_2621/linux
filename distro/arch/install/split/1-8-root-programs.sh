
#!/bin/bash

source install.conf

## görüntü sunucusu xorg 
pacman -Sy \
 xorg-server xorg-xinit xf86-video-intel \
 cpupower \
 networkmanager network-manager-applet \
 ufw \
 pulseaudio pavucontrol pulsemixer \
 i3 \
 dmenu rofi \
 rxvt-unicode konsole \
 qutebrowser \
 nemo pcmanfm ranger dolphin \
 vlc mpv youtube-dl youtube-viewer \
 htop ksysguard \
 flameshot gparted nitrogen lxappearance bc




echo -e "governor='performance'\nmax_freq="2.50Ghz"" > /etc/default/cpupower

## servisler 

systemctl enable NetworkManager
systemctl enable cpupower
systemctl enable ufw
ufw enable


