

source install.conf


echo "/dev/$archboot format ?"
mkfs.fat -F32 /dev/$archboot

echo "/dev/$archroot format ?"
mkfs.ext4 /dev/$archroot

echo "/dev/$archhome format ?"
mkfs.ext4 /dev/$archhome

mkswap /dev/$archswap
swapon /dev/$archswap

mount /dev/$archroot /mnt
mkdir -p /mnt/boot
mkdir -p /mnt/home
mount /dev/$archboot /mnt/boot
mount /dev/$archhome /mnt/home

lsblk -f
