1. Nitrogen -- Unable to locate theme engine in module_path: "adwaita"
    
    `sudo pacman -S gnome-themes-extra`

2. Nitrogen -- Unable to locate theme engine in module_path: "murrine"
    
    `sudo pacman -S gtk-engine-murrine`

3. Ksystemlog -- Icon theme "gnome" not found.
    
    ` sudo pacman -S gnome-icon-theme gnome-icon-theme-extras`

4. Ksystemlog -- Icon theme "Yaru" not found.

    `yay -S yaru-icon-theme-git`

5. Ksystemlog -- Icon theme "Numix-Circle" not found.

    `yay -S numix-circle-icon-theme-git`

6. touchpad tek tıklama ciftıklama çalısmıyor

    `cat /etc/X11/xorg.conf.d/30-touchpad.conf`
    `
    Section "InputClass"
        Identifier "touchpad"
   	        Driver "libinput"
        MatchIsTouchpad "on"
        Option "tapping" "on"
        Option "AccelProfile" "adaptive"
        :Option "TappingButtonMap" "lrm"
    EndSection
    `
7. iphone baglama
    
    mkdir ~/iphone
    sudo idevicepair pair
    sudo ideviceinfo
    ifuse ~/iphone/ -u deviceid

